<?php

/**
 * Return a themed breadcrumb trail.
 *
 * @param $breadcrumb
 *   An array containing the breadcrumb links.
 * @return a string containing the breadcrumb output.
 */
function tinsel_breadcrumb($breadcrumb) {
  if (!empty($breadcrumb)) {
    return '<div class="breadcrumb">'. implode(' › ', $breadcrumb) .'</div>';
  }
}

/**
 * Override or insert tinsel variables into the templates.
 */
function tinsel_preprocess_page(&$vars) {
  $vars['tinsel_classes'] = '';
  if ($vars['left'] != '' && $vars['right'] != '') {
    $vars['tinsel_classes'] = 'sidebars';
  }
  else {
    if ($vars['left'] != '') {
      $vars['tinsel_classes'] .= ' sidebar-left';
    }
    if ($vars['right'] != '') {
      $vars['tinsel_classes'] .= ' sidebar-right';
    }
  }

  // Prepare header
  $site_fields = array();
  if ($vars['site_name']) {
    $site_fields[] = check_plain($vars['site_name']);
  }
  if ($vars['site_slogan']) {
    $site_fields[] = check_plain($$vars['site_slogan']);
  }
  $vars['site_title'] = implode(' ', $site_fields);
  if ($site_fields) {
    $site_fields[0] = '<span>'. $site_fields[0] .'</span>';
  }
  $vars['site_html'] = implode(' ', $site_fields);

  if (function_exists('ctools_menu_secondary_local_tasks')) {
    $tabs = ctools_menu_secondary_local_tasks();
  }
  else {
    $tabs = ctools_menu_secondary_local_tasks();
  }
  $vars['tabs2'] = '';
  if ($tabs) {
    $vars['tabs2'] = '<ul class="tabs secondary">' . $tabs . '</ul>';
  }

  // Hook into color.module
  if (module_exists('color')) {
    _color_page_alter($vars);
  }
}

/**
 * Add a "Comments" heading above comments except on forum pages.
 */
function tinsel_preprocess_comment_wrapper(&$vars) {
  if ($vars['content'] && $vars['node']->type != 'forum') {
    $vars['content'] = '<h2 class="comments">'. t('Comments') .'</h2>'.  $vars['content'];
  }
}

/**
 * Returns the rendered local tasks. The default implementation renders
 * them as tabs. Overridden to split the secondary tasks.
 *
 * @ingroup themeable
 */
function tinsel_menu_local_tasks() {
  if (module_exists('ctools')) {
    ctools_include('menu');
    $tabs = ctools_menu_primary_local_tasks();
  }
  else {
    $tabs = menu_primary_local_tasks();
  }

  if ($tabs) {
    return '<ul class="tabs primary">' . $tabs . '</ul>';
  }
}

function tinsel_comment_submitted($comment) {
  return t('!datetime — !username',
    array(
      '!username' => theme('username', $comment),
      '!datetime' => format_date($comment->timestamp)
    ));
}

function tinsel_node_submitted($node) {
  return t('!datetime — !username',
    array(
      '!username' => theme('username', $node),
      '!datetime' => format_date($node->created),
    ));
}

/**
 * Generates IE CSS links for LTR and RTL languages.
 */
function tinsel_get_ie_styles() {
  global $language;

  $iecss = '<link type="text/css" rel="stylesheet" media="all" href="'. base_path() . path_to_theme() .'/fix-ie.css" />';
  if ($language->direction == LANGUAGE_RTL) {
    $iecss .= '<style type="text/css" media="all">@import "'. base_path() . path_to_theme() .'/fix-ie-rtl.css";</style>';
  }

  return $iecss;
}

/**
 * Override or insert tinsel variables into the templates.
 */
function tinsel_preprocess_pane_header(&$vars) {
  $vars['primary_links'] = menu_primary_links();
  $vars['secondary_links'] = menu_secondary_links();

  $vars['site_name'] = filter_xss_admin(variable_get('site_name', 'Drupal'));
  $vars['site_slogan'] = filter_xss_admin(variable_get('site_slogan', ''));

  // Prepare header
  $site_fields = array();
  if ($vars['site_name']) {
    $site_fields[] = check_plain($vars['site_name']);
  }
  if ($vars['site_slogan']) {
    $site_fields[] = check_plain($vars['site_slogan']);
  }
  $vars['site_title'] = implode(' ', $site_fields);
  if ($site_fields) {
    $site_fields[0] = '<span>'. $site_fields[0] .'</span>';
  }
  $vars['site_html'] = implode(' ', $site_fields);
}

/**
 * Override or insert tinsel variables into the templates.
 */
function tinsel_preprocess_pane_messages(&$vars) {
  $vars['tabs'] = theme('menu_local_tasks');
  $tabs = ctools_menu_secondary_local_tasks();
  $vars['tabs2'] = '';
  if ($tabs) {
    $vars['tabs2'] = '<ul class="tabs secondary">' . $tabs . '</ul>';
  }

  $vars['title'] = drupal_get_title();
  $vars['mission'] = '';
  // Set mission when viewing the frontpage.
  if (drupal_is_front_page()) {
    $vars['mission'] = filter_xss_admin(theme_get_setting('mission'));
  }
  $vars['breadcrumb'] =  theme('breadcrumb', drupal_get_breadcrumb());
}
